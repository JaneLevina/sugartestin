<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class HomeController extends Controller
{
  /**
   * Показать профиль данного пользователя.
   *
   * @param  int  $id
   * @return Response
   */
  public function home()
  {
    return view('home');
  }

  public function checkCaptcha(Request $request)
  {
    $rules = ['captcha' => 'required|captcha'];
    $validator = Validator::make($request->all(), $rules);

    return json_encode(['error' => $validator->fails()]);
  }

  public function ajaxSaveMessage(Request $request)
  {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, config('app.out_server') . 'message');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($request->all()));
    $out = curl_exec($curl);
    curl_close($curl);

    return $out;
  }
}