<script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="bower_components/moment/min/moment.min.js"></script>
<script type="text/javascript" src="bower_components/moment/locale/ru.js"></script>
<script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="bower_components/bootstrap-material-design/dist/js/material.min.js"></script>
<script type="text/javascript" src="bower_components/bootstrap-material-design/dist/js/ripples.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>

<link href="bower_components/bootstrap/dist/css/bootstrap.css" type="text/css" rel="stylesheet" />
<link href="bower_components/bootstrap-material-design/dist/css/bootstrap-material-design.min.css" type="text/css" rel="stylesheet" />
<link href="bower_components/bootstrap-material-design/dist/css/ripples.css" type="text/css" rel="stylesheet" />
<link href="css/main.css" type="text/css" rel="stylesheet" />