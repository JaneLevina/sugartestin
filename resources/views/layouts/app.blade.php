<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Laravel Quickstart - Sugar Test In</title>

    @include('layouts/scripts')

    <!-- CSS и JavaScript -->
  </head>

  <body>
    <div class="container">
      @yield('content')
    </div>

    <script  type="text/javascript">
        jQuery(document).ready(function($){
            $.material.init();                        
        });
    </script>
  </body>
</html>

