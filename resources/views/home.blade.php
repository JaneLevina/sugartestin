@extends('layouts.app')

@section('content')

  <div class="panel-body">

    @include('common.errors')

    <form action="/store" method="POST" class="form-horizontal" id='store_form'>
      {{ csrf_field() }}
      <div class="form-group">
        <label for="task" class="col-sm-3 control-label">Name</label>
        <div class="col-sm-6">
          <input type="text" name="name" id="name" class="form-control">
        </div>
      </div>
      <div class="form-group">
        <label for="task" class="col-sm-3 control-label">Message</label>
        <div class="col-sm-6">
          <textarea type="text" name="message" id="message" class="form-control"></textarea>
        </div>
      </div>
      <div class="form-group">
        <label for="task" class="col-sm-3 control-label">Email</label>
        <div class="col-sm-6">
          <input type="text" name="email" id="email" class="form-control">
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
          <button type="submit" class="btn btn-default">Submit</button>
          <button type="reset" class="btn btn-default">Reset</button>
        </div>
      </div>
    </form>

    <div class="modal fade" id="captcha">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">CAPTCHA</h4>
          </div>
          <div class="modal-body">
          
            <form method="post" action="" id="check-captcha">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group">
                <p><?= Captcha::img() ?></p>
                <input type="text" name="captcha" class="form-control" autocomplete="off">
              </div>
              <button type="submit" name="check" class="btn btn-default">Check</button>
            </form>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="notify">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">NOTIFY</h4>
          </div>
          <div class="modal-body"><p></p></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
          </div>
        </div>
      </div>
    </div>


  </div>

@endsection