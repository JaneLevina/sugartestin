jQuery(document).ready(function($){
	$.material.init();

	$('#store_form').validate({
    rules: {
      name: "required",
      email: {
        required: true,
        email: true
      },
      message: "required"
    },
    submitHandler: function(form) {
        $('#captcha').modal('show');
    }
  });

	$(document).on('click', '#check-captcha button', function(event) {
		event.preventDefault();
		$.ajax({
			url: '/check-captcha',
			type: 'POST',
			data: $('#check-captcha').serialize(),
			dataType: 'json'
		}).done(function(data) {			console.log(data);

			if (data.error) {
				$('#check-captcha .form-group').addClass('has-error');
			} else {
				$('#captcha').modal('hide')

				$.ajax({
					url: '/save-message',
					type: 'POST',
					data: $('#store_form').serialize(),
					dataType: 'json'
				}).done(function(out_data) {
					if (out_data.error) {
						$('#notify .modal-body p').addClass('bg-danger').text(out_data.message);
						$('#notify').modal('show');
					} else {
						$('#notify .modal-body p').addClass('bg-success').text('SUCCESS!');
						$('#notify').modal('show')
					}
					
				});		
			}

		});
	});


});
